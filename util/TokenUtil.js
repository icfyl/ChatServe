import jwt from 'jsonwebtoken'
import config from '../config'

/*
* @param param 要签名的参数
* @param time 失效时间单位秒
* */
export const sign = function(userId, time = 1){
  const payload = {userId}
  let token = jwt.sign(payload, config.privateKey, {expiresIn: time});
  return token
}

export const verify = function(token){
  let result = {};
  jwt.verify(token, config.privateKey, (error, decoded)=>{
    if(error){
      result.success =  false
    }else{
      result.success = true
      result.decoded = decoded
    }
  });
  return result
}


