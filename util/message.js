export function message(errno = 0, msg = "", data = {}) {
  return{
    errno,
    msg,
    data
  }
}
