# ChatServe

#### 介绍
使用 node 和 moogodb 开发聊天服务后台
注意，目前正在重构, old 分支还是原来的


#### 软件架构
1. node
2. express
3. socket
4. moogodb
5. moogoose



#### 安装教程

1.  git clone https://gitee.com/icfyl/ChatServe.git
2.  cd ChatServe
3.  npm install

#### 使用说明

1.  生成接口文档 apidoc -i routes/ -o public/apidoc/
2.  node index.js 启动项目

#### 重构进度
1. 重写了登录注册接口

