import express from 'express'
import Message from '../models/message'
import {auth} from '../middleware'

const router = express.Router();

//获取聊天历史消息
router.get('/getMessageHistory', auth,async function (req, res, next) {
  const {roomId, messageId, page }= req.query
  if(messageId === -1){
    const messageList = await Message.find({roomId, messageId : {$lt : messageId}})
      .skip(page*10)
      .limit(10)
      .sort({'_id':-1});

    res.json({
      errno: 0,
      data: messageList,
      msg: '获取历史消息成功'
    })
  }else{
    const messageList = await Message.find({roomId})
      .skip(page*10)
      .limit(10)
      .sort({'_id':-1});

    res.json({
      errno: 0,
      data: messageList,
      msg: '获取历史消息成功'
    })
  }


})

router.get('/deleteMessageHistory', auth, async function (req, res, nex) {
  const roomId = req.query.roomId
  await Message.remove({'roomId': roomId});

  res.json({
    errno: 0,
    msg: '删除历史消息成功'
  })
})

export default router
