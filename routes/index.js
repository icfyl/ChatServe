/**
 * 作者： liushun
 * 时间： 2020/5/17
 * 功能： 总路由
 * **/

import user from './user';
import message from './message'
import friend from './friend'
import upload from './upload'



export default app => {
  app.use('/api/user',  user);
  app.use('/api/message', message);
  app.use('/api/friend', friend);
  app.use('/api/upload', upload);
}
