/**
 * 作者： liushun
 * 时间： 2020/5/17
 * 功能： 用户模型
 * **/

import mongoose from 'mongoose'


const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: {
    type: String,
    unique: true
  },
  password: {
    type: String,
  },
  sex: {
    type: Number,
    default: 0,
  }, // 性别 0-男  1-女
  avatar: {     //头像
    type: String,
    default: 'default.jpeg'
  },
  address: {
    type: String,
    default: '中国'
  }, //地区
  sign: {
    type: String,
    default: '这个人还没有签名'
  }, //签名
  letter: {
    type: String, //用户名首字母大写 用于前端排序
  },
  createAt: {
    type: Date,
    default: Date.now()
  },
  updateAt: {
    type: Date,
    default: Date.now()
  }
})



const User = mongoose.model('User', userSchema);

export default User
