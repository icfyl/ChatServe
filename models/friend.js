import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const friendSchema = new Schema({
  selfId: {
    type: String,
  },
  friendId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  isDelete: {
    type: Number,
    default: 0
  },
  time: {
    type: Date,
    default: Date.now()
  }
})



const Friend = mongoose.model('Friend', friendSchema);

export default Friend
