import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const thumbUpSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  articleId:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Article'
  },
  time: {
    type: Date,
    default: Date.now()
  }
})


const ThumbUp = mongoose.model('ThumbUp', thumbUpSchema);

export default ThumbUp
