import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const commentSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  articleId:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Article'
  },
  commentContent: {
    type: String,
  },
  time: {
    type: Date,
    default: Date.now()
  }
})


const Comment = mongoose.model('Comment', commentSchema);

export default Comment
