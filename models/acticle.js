import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const articleSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  articleContent: {
    type: String,
  },
  time: {
    type: String,
  }
})


const Article = mongoose.model('Article', articleSchema);

export default Article
