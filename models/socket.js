import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const socketSchema = new Schema({
  socketId: {
    type: String,
    unique: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  time: {
    type: Date,
    default: Date.now()
  }
})


const Socket = mongoose.model('Socket', socketSchema);

export default Socket
