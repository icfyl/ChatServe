import mongoose from 'mongoose'

const Schema = mongoose.Schema;

const messageSchema = new Schema({
  username: String,
  userId: String,
  avatar: String,
  roomId: {
    type: String,
  },
  msg: {
    type: String,
    default: ''
  },
  type: {
    type: String,
  },
  duration:{
    type: Number,
  },
  time: {
    type: Date,
    default: Date.now()
  },
  messageId: {
    type: Number,
  }
})


const Message = mongoose.model('Message', messageSchema);

export default Message
