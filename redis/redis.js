/**
 * 作者： liushun
 * 时间： 2020/5/17
 * 功能： redis 初始化
 * **/


import redis from 'redis';
const client = redis.createClient( 6379, '127.0.0.1');

client.on("error", function(error) {
  global.logger.error(error);
});

client.on("connect", function(success) {
  global.logger.error(success);
});
