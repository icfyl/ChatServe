import {verify} from "../util/TokenUtil";

export function auth(req,res,next){

  const access_token = req.get('Authorization').split(' ').pop()

  console.log(access_token)

  if(verify(access_token).success){
    next();
  }else{
    res.json({
      errno: 2,
      msg: '您没有权限访问'
    })
  }
}
