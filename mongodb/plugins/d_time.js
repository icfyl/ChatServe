/**
 * 作者： liushun
 * 时间： 2020/5/17
 * 功能： mongoose 更改数据创建时间和最后修改时间
 * **/

module.exports = exports = function lastModifiedPlugin (schema, options) {

  schema.pre('save', function (next) {
    this.lastMod = new Date();
    next();
  });

  if (options && options.index) {
    schema.path('lastMod').index(options.index);
  }
}
